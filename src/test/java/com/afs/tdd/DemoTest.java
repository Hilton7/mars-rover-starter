package com.afs.tdd;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DemoTest {
    @Test
    void should_change_location_to_0_1_North_when_executeCommand_given_location_0_0_North_and_command_Move() {
        //given
        Location location = new Location(0, 1, Direction.N);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.N));
        //when
        masRover.receiveCommand(Command.M);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }

    @Test
    void should_change_location_to_0__1_South_when_executeCommand_given_location_0_0_South_and_command_Move() {
        //given
        Location location = new Location(0, -1, Direction.S);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.S));
        //when
        masRover.receiveCommand(Command.M);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }

    @Test
    void should_change_location_to_1_0_East_when_executeCommand_given_location_0_0_East_and_command_Move() {
        //given
        Location location = new Location(1, 0, Direction.E);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.E));
        //when
        masRover.receiveCommand(Command.M);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to__1_0_West_when_executeCommand_given_location_0_0_West_and_command_Move() {
        //given
        Location location = new Location(-1, 0, Direction.W);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.W));
        //when
        masRover.receiveCommand(Command.M);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_North_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.W);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.N));
        //when
        masRover.receiveCommand(Command.L);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to_0_0_East_when_executeCommand_given_location_0_0_South_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.E);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.S));
        //when
        masRover.receiveCommand(Command.L);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }

    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.N);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.E));
        //when
        masRover.receiveCommand(Command.L);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }

    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_West_and_command_Left() {
        //given
        Location location = new Location(0, 0, Direction.S);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.W));
        //when
        masRover.receiveCommand(Command.L);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }

    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_East_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.E);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.N));
        //when
        masRover.receiveCommand(Command.R);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to_0_0_West_when_executeCommand_given_location_0_0_South_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.W);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.S));
        //when
        masRover.receiveCommand(Command.R);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to_0_0_South_when_executeCommand_given_location_0_0_East_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.S);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.E));
        //when
        masRover.receiveCommand(Command.R);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_Right() {
        //given
        Location location = new Location(0, 0, Direction.N);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.W));
        //when
        masRover.receiveCommand(Command.R);
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());

    }


    @Test
    void should_change_location_to_0_0_North_when_executeCommand_given_location_0_0_West_and_command_Move_and_Left_and_Right() {
        //given
        Location location = new Location(0, 1, Direction.N);
        MasRover masRover = new MasRover(new Location(0, 0, Direction.N));
        Command commands[] = {Command.M, Command.L, Command.R};
        //when
        masRover.receiveListCommands(List.of(commands));
        //then
        assertEquals(location.toString(), masRover.getLocation().toString());
    }


}
