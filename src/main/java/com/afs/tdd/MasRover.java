package com.afs.tdd;

import java.util.List;
import java.util.Objects;

public class MasRover {
    private Location location;
    private int step;

    public MasRover(Location location) {
        this.step = 1;
        this.location = location;
        if (this.location.direction.equals(Direction.S) || this.location.direction.equals(Direction.W))
            this.step = -1;
    }

    private void move() {
        if (this.location.direction.equals(Direction.N) || this.location.direction.equals(Direction.S)) {
            this.location.y += step;
        } else if (this.location.direction.equals(Direction.W) || this.location.direction.equals(Direction.E)) {
            this.location.x += step;
        }
    }

    private void divert(Command command) {
        if (command.equals(Command.L)) {
            if (this.location.direction == Direction.N) {
                this.location.direction = Direction.W;
                this.step = -1;
            } else if (this.location.direction == Direction.W) {
                this.location.direction = Direction.S;
                this.step = -1;
            } else if (this.location.direction == Direction.S) {
                this.location.direction = Direction.E;
                this.step = 1;
            } else if (this.location.direction == Direction.E) {
                this.location.direction = Direction.N;
                this.step = 1;
            }
        } else if (command.equals(Command.R)) {
            if (this.location.direction == Direction.N) {
                this.location.direction = Direction.E;
                this.step = 1;
            } else if (this.location.direction == Direction.W) {
                this.location.direction = Direction.N;
                this.step = 1;
            } else if (this.location.direction == Direction.S) {
                this.location.direction = Direction.W;
                this.step = -1;
            } else if (this.location.direction == Direction.E) {
                this.location.direction = Direction.S;
                this.step = -1;
            }
        }
    }

    public void receiveCommand(Command command) {
        if (Objects.isNull(command)) {
            return;
        }

        if (command.equals(Command.M)) {
            this.move();
        } else if (command.equals(Command.L) || command.equals(Command.R)) {
            this.divert(command);
        }
    }

    public void printStatus() {
        System.out.println(String.format("(%d,%d,%s)", this.location.x, this.location.y, this.location.direction.name()));
    }

    public Location getLocation() {
        return location;
    }

    public void receiveListCommands(List<Command> commands) {
        commands.stream().forEach(command -> receiveCommand(command));
    }
}
