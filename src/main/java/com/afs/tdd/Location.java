package com.afs.tdd;

public class Location {
    public Direction direction;
    public int x;
    public int y;

    public Location(int x, int y, Direction direction) {
        this.direction = direction;
        this.x = x;
        this.y = y;
    }

    public Location() {
    }

    @Override
    public String toString() {
        return "Location{" +
                "direction=" + direction +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
