# **What did we learn today? What activites did you do? What scenes have impressed you**
1.code review. In code review, I have a deeper understanding of the design idea of observer mode.
2.junit learning, through junit learning, I understand the basic steps of unit testing, so that I can complete the code writing and test the unit more standardized in the future coding process.
3.TDD, TDD is a relatively new concept. To me, in the traditional mode, we generally complete the development of functions before testing, while TDD emphasizes the completion of the construction of the test framework first, and the development is driven by the test framework. After my experiment, I found that this mode can make me better understand the function of the unit and some edge conditions in the process of function coding, and enable me to consider more rigorously in the coding proces,and TDD impresses me most.
# **Pleas use one word to express your feelings about today's class.**
useful
# **What do you think about this? What was the most meaningful aspect of this activity?**
Today's content is very substantial, and among them I think the most beneficial module should be TDD, it brings a new programming idea.
# **Where do you most want to apply what you have learned today? What changes will you make?**
Today, I learned the writing of junit and the idea of TDD programming, which changed the way I used to code first and then program, and let me know that it is more detailed to consider programming from testing.